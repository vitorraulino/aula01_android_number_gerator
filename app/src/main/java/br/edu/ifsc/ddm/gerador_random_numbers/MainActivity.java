package br.edu.ifsc.ddm.gerador_random_numbers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sortearNumero(View vw) {
        TextView tv = (TextView) findViewById(R.id.textView3);
        EditText t1 = (EditText) findViewById(R.id.editText);
        EditText t2 = (EditText) findViewById(R.id.editText2);
        String msg_tv = "";
        int num_inicial = 0;
        int num_final = 0;
        int num_resultado = 0;
        if(t1.getText().toString().equals("")){
            msg_tv = "Informe o primeiro número do intervalo!";
            tv.setText(msg_tv.toString());
            return;
        }
        if(t2.getText().toString().equals("")){
            msg_tv = "Informe o segundo número do intervalo!";
            tv.setText(msg_tv.toString());
            return;
        }

        num_inicial = Integer.parseInt(t1.getText().toString());
        num_final = Integer.parseInt(t2.getText().toString());

        if(num_final < num_inicial){
            msg_tv = "Número inicial deve ser menor do que o final!";
            tv.setText(msg_tv.toString());
            return;
        }
        if(num_final > 100){
            msg_tv = "Número final não deve ser maior que 100!";
            tv.setText(msg_tv.toString());
            return;
        }
        Random r = new Random();
        num_resultado = r.nextInt(num_final - num_inicial) + num_inicial;
        msg_tv = "" + num_resultado;

        tv.setText(msg_tv.toString());
    }

}
